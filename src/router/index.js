import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
  },
  {
    path: '/add',
    name: 'AddProduct',
    component: () => import(/* webpackChunkName: "add_product" */ '../views/AddProduct.vue')
  },
  {
    path: '/verify',
    name: 'VerifyCart',
    component: () => import(/* webpackChunkName: "add_product" */ '../views/VerifyCart.vue')
  },
  // {
  //   path: '/my_carts',
  //   name: 'MyCarts',
  //   component: () => import(/* webpackChunkName: "add_product" */ '../views/MyCart.vue'),
  //   children: [
  //     {
  //       path: 'open',
  //       name: 'MyCartsOpen',
  //       component: () => import(/* webpackChunkName: "add_product" */ '../components/Cart/user/AllCarts.vue'),
  //     },
  //     {
  //       path: 'closed',
  //       name: 'MyCartsClosed',
  //       component: () => import(/* webpackChunkName: "add_product" */ '../components/Cart/user/AllCarts.vue'),
  //     },
  //   ]
  // },
  {
    path: '/my_carts',
    name: 'MyCarts',
    component: () => import(/* webpackChunkName: "add_product" */ '../views/MyCart.vue'),
    children: [
      {
        path: ':kind',
        props: true,
        name: 'MyCartsGeneral',
        component: () => import(/* webpackChunkName: "add_product" */ '../components/Cart/user/AllCarts.vue'),
      },
      
    ]
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
