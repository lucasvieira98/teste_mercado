// import Api from '../../api/index'

const Cart = {
  namespaced: true,
  state: {
    allCarts: [], // [],

    oneCart: {
      id: 1,
      owner: '',
      n_items: 0,
      items: [],
      value: 0,
    }, // {}

    success: null,
    success_msg: null,
    error: null,
    error_msg: null,
  },
  
  getters: {
    getAllCarts: state => {
      return state.allCarts;
    },
    getVerifiedCarts: state => {
      return state.allCarts.filter(o=>o.verified);
    },
    getOpenCarts: state => {
      return state.allCarts.filter(o=>!o.verified);
    },
    getOneCartItems: state => {
      return state.oneCart.items;
    },
  },

  mutations: {
    addItem(state, item){
      let idx= state.oneCart.items.map(item=>item.name.toUpperCase()).indexOf(item.name.toUpperCase())
      if (idx == -1) {
        state.oneCart.items.push({...item, qtd: 1})
        state.oneCart.n_items = state.oneCart.items.length
        state.oneCart.value += item.value
        localStorage.setItem('one_cart', JSON.stringify(state.oneCart))
      } else {
        state.oneCart.items[idx].qtd++
        state.oneCart.value += item.value
        localStorage.setItem('one_cart', JSON.stringify(state.oneCart))
        alert(`O produto ${item.name.toUpperCase()} já foi adicionado ao carrinho. Acrescentamos 1 na quantidade total do item.`)
      }
    },
    removeItem(state, item) {
      let index = state.oneCart.items.indexOf(item)
      state.oneCart.items.splice(index, 1)
      state.oneCart.n_items--
      state.oneCart.value -= item.value
      localStorage.setItem('one_cart', JSON.stringify(state.oneCart))
    },
    edit_qtd_cart(state, {edit}) {
      let index = state.oneCart.items.map(edit=>edit.name.toUpperCase()).indexOf(edit.name.toUpperCase())
      state.oneCart.items[index] = {...edit}
      state.oneCart.value = 0
      for (let item of state.oneCart.items) {
        state.oneCart.value += (item.value * (item.qtd))
      }
      localStorage.setItem('one_cart', JSON.stringify(state.oneCart))
    },
    setOneCart(state, items) {
      if (items == null) {
        localStorage.setItem('one_cart', JSON.stringify(state.oneCart))
      }
      else {
        state.oneCart = items
        
      }
    },
    setAllCarts(state, items) {
      if (items == null) {
        localStorage.setItem('all_carts', JSON.stringify(state.allCarts))
      }
      else {
        state.allCarts = items
      }
    },
    addToAllCarts(state, owner) {
      if (owner != null) {
        let one_cart = JSON.parse(localStorage.getItem('one_cart'))
        one_cart.owner = owner
        one_cart.id = state.allCarts.length
        state.allCarts.push(one_cart)
        localStorage.setItem('all_carts', JSON.stringify(state.allCarts))
        Object.assign(state.oneCart, {id: 0, owner: '', n_items: 0, items: [], value: 0})
        localStorage.setItem('one_cart', JSON.stringify(state.oneCart))
      }
    },
    approveItem(state, {item, indexCart, indexItem}) {
      Object.assign(state.allCarts[indexCart].items[indexItem], {...item, approved: true, reason: ''})
      let countApproved = 0
      let countDisapproved = 0
      for (let product of state.allCarts[indexCart].items) {
        if (product.approved == true) {
          countApproved++
        } else if (product.approved == false) {
          countDisapproved++
        }
      }
      if ((countApproved + countDisapproved) == state.allCarts[indexCart].items.length) {
        Object.assign(state.allCarts[indexCart], {...state.allCarts[indexCart], verified: true})
        if (countApproved == state.allCarts[indexCart].items.length) {
          state.allCarts[indexCart].allApproved = true
        }
        setTimeout(() => {
          location.reload() 
        }, 2000);
      }
      localStorage.setItem('all_carts', JSON.stringify(state.allCarts))
    },
    reproveItem(state, {item, indexCart, indexItem, reason}) {
      Object.assign(state.allCarts[indexCart].items[indexItem], {...item, approved: false, reason: reason})
      let countApproved = 0
      for (let product of state.allCarts[indexCart].items) {
        if (product.approved == true || product.approved == false) {
          countApproved++
        }
      }
      if (countApproved == state.allCarts[indexCart].items.length) {
        Object.assign(state.allCarts[indexCart], {...state.allCarts[indexCart], verified: true})
        location.reload()
      }
      localStorage.setItem('all_carts', JSON.stringify(state.allCarts))
    },
    approveAll(state, {indexCart}) {
      state.allCarts[indexCart].items = state.allCarts[indexCart].items.map(o=> {return {...o, approved: true, reason: ''}})
      state.allCarts[indexCart].verified = true
      state.allCarts[indexCart].allApproved = true
      localStorage.setItem('all_carts', JSON.stringify(state.allCarts))
    },
    reproveAll(state, {indexCart, reason}) {
      state.allCarts[indexCart].items = state.allCarts[indexCart].items.map(o=> {return {...o, approved: false, reason: reason}})
      state.allCarts[indexCart].verified = true
      localStorage.setItem('all_carts', JSON.stringify(state.allCarts))
    },
    editFromVerified(state, {item, indexItem, indexCart}) {
      console.log(item)
      if (item.name == state.allCarts[indexCart].items[indexItem].name) {
        state.allCarts[indexCart].items[indexItem].qtd = Number(item.qtd)
        state.allCarts[indexCart].items[indexItem].approved = undefined
        state.allCarts[indexCart].verified = false
        for (let cart of state.allCarts) {
          cart.value = 0
          for (let item of cart.items) {
            cart.value += (item.value * item.qtd)
          }
        }
      } else {
        let newIndex = state.allCarts[indexCart].items.map(o=>o.name).indexOf(item.name)
        if (newIndex > -1) {
          state.allCarts[indexCart].items[newIndex].qtd += Number(item.qtd)
          state.allCarts[indexCart].items[newIndex].approved = undefined
          state.allCarts[indexCart].items[newIndex].reason = undefined
          state.allCarts[indexCart].verified = false
          state.allCarts[indexCart].items.splice(indexItem, 1)
          state.allCarts[indexCart].value = 0
          for (let cart of state.allCarts) {
            cart.value = 0
            for (let item of cart.items) {
              cart.value += (item.value * item.qtd)
            }
          }
        }
      }
      localStorage.setItem('all_carts', JSON.stringify(state.allCarts))      
    },
    deleteFromVerified(state, {item, indexCart}) {
      let index = state.allCarts[indexCart].items.map(item=>item.name.toUpperCase()).indexOf(item.name.toUpperCase())
      state.allCarts[indexCart].items.splice(index, 1)
      let checkIfAllApproved = state.allCarts[indexCart].items.filter(o=> o.approved == false).length
      if (checkIfAllApproved == 0) {
        state.allCarts[indexCart].allApproved = true
        setTimeout(() => {
          location.reload()
        }, 2000);
      }
      if(state.allCarts[indexCart].items.length == 0){
        state.allCarts.splice(indexCart,1)
      }
      localStorage.setItem('all_carts', JSON.stringify(state.allCarts))
    },
  },

  actions: {
    setOneItem(context, item) {
      context.commit('addItem', item)
    },
    removeOneItem(context, item) {
      context.commit('removeItem', item)
    },
    pushToAllCarts(context, owner) {
      context.commit('addToAllCarts', owner)
    },
    approveSingleItem(context, {item, indexCart, indexItem}) {
      context.commit('approveItem', {item, indexCart, indexItem})
    },
    reproveSingleItem(context, {item, indexCart, indexItem, reason}) {
      context.commit('reproveItem', {item, indexCart, indexItem, reason})
    },
    approveAllItems(context, {indexCart}) {
      context.commit('approveAll', {indexCart})
    },
    reproveAllItems(context, {indexCart, reason}) {
      context.commit('reproveAll', {indexCart, reason})
    },
    edit_qtd(context, {edit}) {
      context.commit('edit_qtd_cart', {edit})
    },
    editVerified(context, {item, indexItem, indexCart}) {
      context.commit('editFromVerified', {item, indexItem, indexCart})
    },
    deleteVerified(context, {item, indexCart}) {
      context.commit('deleteFromVerified', {item, indexCart})
    },

    index(context){
      let one_cart_local_storage = JSON.parse(localStorage.getItem('one_cart'))
      context.commit('setOneCart', one_cart_local_storage)
      let all_carts_local_storage = JSON.parse(localStorage.getItem('all_carts'))
      context.commit('setAllCarts', all_carts_local_storage)
    },
    show(){},
    create(){},
    update(){},
    destroy(){},
  }

};

export default Cart;