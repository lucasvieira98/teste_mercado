// import Api from '../../api/index'

const Product = {
  namespaced: true,
  state: {
    allProducts: null, // [],

    newProducts: null, // [],

    oneProduct: null, // {}

    success: null,
    success_msg: null,
    error: null,
    error_msg: null,
  },
  getters: {
    getAllProducts: state => {
      return state.allProducts
    }
  },

  mutations: {
    editItem(state, {item, index}){
      Object.assign(state.allProducts[index], item)
      localStorage.setItem('Products', JSON.stringify(state.allProducts))
    },
    setOneItem(state, {item, index}){
      if (index > -1) {
        if (state.allProducts.map(item=>item.name.toUpperCase()).indexOf(item.name.toUpperCase()) == -1) {
          Object.assign(state.allProducts[index], item)
          localStorage.setItem('Products', JSON.stringify(state.allProducts))
        } else if (state.allProducts[index].value != item.value ){
          Object.assign(state.allProducts[index], item)
          localStorage.setItem('Products', JSON.stringify(state.allProducts))
          alert(`O produto ${item.name.toUpperCase()} teve seu valor modificado.`)
        } else if (state.allProducts[index].category != item.category) {
          Object.assign(state.allProducts[index], item)
          localStorage.setItem('Products', JSON.stringify(state.allProducts))
          alert(`O produto ${item.name.toUpperCase()} teve sua categoria modificada.`)
        }
      } else {
        if (state.allProducts.map(item=>item.name.toUpperCase()).indexOf(item.name.toUpperCase()) == -1) {
          state.allProducts.push(item)
          localStorage.setItem('Products', JSON.stringify(state.allProducts))
        } else {
          alert(`O produto ${item.name.toUpperCase()} já foi adicionado a lista.`)
        }
      }
    },
    setAllItems(state, items){
      state.allProducts = items // items.data
      localStorage.setItem('Products', JSON.stringify(state.allProducts))
    },
    deleteOneItem(state, item) {
      if (confirm(`Deseja realmente deletar o produto ${item.name.toUpperCase()}?`)) {
        let index = state.allProducts.indexOf(item)
        state.allProducts.splice(index, 1)
        let aux_storage = JSON.parse(localStorage.getItem('Products'))
        aux_storage.splice(index, 1)
        localStorage.setItem('Products', JSON.stringify(aux_storage))
      }
    },
    closeDialogBox(state) {
      for (let n = 0; n < state.allProducts.length; n++) {
        state.allProducts[n] = { ...state.allProducts[n], editing: false }
      }
    }
  },

  actions: {

    addOneItem(context, {item, index}) {
      context.commit('setOneItem', {item, index})
    },
    editOneItem(context, {item, index}) {
      context.commit('editItem', {item, index})
    },
    deleteOneProduct(context, item) {
      context.commit('deleteOneItem', item)
    },
    close(context) {
      context.commit('closeDialogBox')
    },

    index(context){
      let products_local_storage = JSON.parse(localStorage.getItem('Products'))
      let products_no_banco_de_dados = [
        {name: 'Queijo', value: 10, category: 'Laticínios'},
        {name: 'Banana', value: 10, category: 'Frutas'},
        {name: 'Pão', value: 10, category: 'Padaria'},
        {name: 'Goaiba', value: 10, category: 'Frutas'},
      ]

      if (products_local_storage == null) {
        context.commit('setAllItems', products_no_banco_de_dados)
      } else {
        context.commit('setAllItems', products_local_storage)
      }
    },
    show(){},
    create(){},
    update(){},
    destroy(){},
  }

};

export default Product;